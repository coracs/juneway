FROM debian:9 as builder

WORKDIR /build

RUN apt update \
	&& apt upgrade -y \
	&& apt install -y curl gcc make libpcre3-dev libssl-dev zlib1g-dev libgetopt-lucid-perl

RUN curl -o openresty-1.21.4.1.tar.gz https://openresty.org/download/openresty-1.21.4.1.tar.gz \
	&& tar -zxf openresty-1.21.4.1.tar.gz 

RUN cd openresty-1.21.4.1 && ./configure --modules-path=/usr/lib/nginx/modules \
--conf-path=/etc/nginx/nginx.conf \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--pid-path=/var/run/nginx.pid \
--lock-path=/var/run/nginx.lock \
--http-client-body-temp-path=/var/cache/nginx/client_temp \
--http-proxy-temp-path=/var/cache/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
--http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
--http-scgi-temp-path=/var/cache/nginx/scgi_temp \
--with-compat \
--with-file-aio \
--with-threads \
--with-http_addition_module \
--with-http_auth_request_module \
--with-http_dav_module \
--with-http_flv_module \
--with-http_gunzip_module \
--with-http_gzip_static_module \
--with-http_mp4_module \
--with-http_random_index_module \
--with-http_realip_module \
--with-http_secure_link_module \
--with-http_slice_module \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_sub_module \
--with-http_v2_module \
--with-mail \
--with-mail_ssl_module \
--with-stream \
--with-stream_realip_module \
--with-stream_ssl_module \
--with-stream_ssl_preread_module \
	&& make \
	&& make install

FROM debian:9 as runner
COPY --from=builder ["/etc/nginx/", "/etc/nginx/"]
COPY --from=builder ["/usr/local/openresty/", "/usr/local/openresty/"]
COPY --from=builder ["/usr/lib/x86_64-linux-gnu/libssl.so.1.1", "/usr/lib/x86_64-linux-gnu/libcrypto.so.1.1", "/usr/local/openresty/luajit/lib/libluajit-5.1.so.2", "/usr/lib/"]

RUN mkdir -p /var/cache/nginx/client_temp/ \
	&& mkdir -p /var/log/nginx \
	&& touch /var/log/nginx/access.log /var/log/nginx/error.log \
	&& ln -sf /dev/stdout /var/log/nginx/access.log \
    	&& ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80

CMD ["/usr/local/openresty/nginx/sbin/nginx", "-g", "daemon off;"]
