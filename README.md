# Overview

### Build
 - docker build -t nginx:custom .
 - docker build -f Dockerfile.lua -t nginx:lua .

### Run
 - docker run -v "$(pwd)/docker/custom/nginx.conf:/etc/nginx/nginx.conf:ro" -dp 8000:80 nginx:custom 
 - docker run -v "$(pwd)/docker/lua/nginx.conf:/etc/nginx/nginx.conf:ro" -dp 8001:80 nginx:lua

### Test custom
 - $ curl -I http://127.0.0.1:8000
```
HTTP/1.1 200 OK
Server: nginx/1.23.3
Date: Thu, 16 Mar 2023 13:40:02 GMT
Content-Type: text/html
Content-Length: 615
Last-Modified: Wed, 15 Mar 2023 21:11:01 GMT
Connection: keep-alive
ETag: "64123465-267"
Accept-Ranges: bytes 
```

### Test Lua
```
        location /hello {
                content_by_lua_block {
                        local hello = string.format('Hello, %s!', ngx.var.remote_addr)
                        ngx.say(hello);
                }
        }
```
 - $ curl http://127.0.0.1:8001/hello

```
HTTP/1.1 200 OK
Server: openresty/1.21.4.1
Date: Thu, 16 Mar 2023 14:24:40 GMT
Content-Type: application/octet-stream
Connection: keep-alive
```

```
Hello, 172.17.0.1! 
```
